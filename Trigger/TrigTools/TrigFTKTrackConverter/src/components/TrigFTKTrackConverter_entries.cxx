#include "TrigFTKTrackConverter/TrigFTKTrackConverter.h"
#include "TrigFTKTrackConverter/TrigFTKClusterConverterTool.h"
#include "TrigFTKTrackConverter/TrigFTKUncertiantyTool.h"

DECLARE_COMPONENT( TrigFTKTrackConverter )
DECLARE_COMPONENT( TrigFTKClusterConverterTool )
DECLARE_COMPONENT( TrigFTKUncertiantyTool )

