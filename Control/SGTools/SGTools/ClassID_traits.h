/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SGTOOLS_CLASSID_TRAITS_H
#define SGTOOLS_CLASSID_TRAITS_H
/** @file ClassID_traits.h
 * @brief  a traits class that associates a CLID to a type T
 * It also detects whether T inherits from Gaudi DataObject
 *
 * @author Paolo Calafiura <pcalafiura@lbl.gov> - ATLAS Collaboration
 * $Id: ClassID_traits.h,v 1.3 2009-01-15 19:07:29 binet Exp $
 */


#include "AthenaKernel/ClassID_traits.h"


#endif // not SGTOOLS_CLASSID_TRAITS_H






